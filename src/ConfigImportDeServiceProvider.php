<?php

namespace Drupal\config_import_de;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Overrides config import validator class to delete any entities if needed.
 */
class ConfigImportDeServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('entity.bundle_config_import_validator');
    $definition->setClass('Drupal\config_import_de\Event\ConfigImportDeBundleConfigImportValidate');
    $definition->addArgument(new Reference('messenger'));
    $definition->addArgument(new Reference('config.factory'));
  }

}
