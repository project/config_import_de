<?php

namespace Drupal\config_import_de\Event;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Event\BundleConfigImportValidate;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Entity config importer validation event subscriber.
 */
class ConfigImportDeBundleConfigImportValidate extends BundleConfigImportValidate {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs the event subscriber.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigManagerInterface $config_manager, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, ConfigFactoryInterface $config_factory) {
    parent::__construct($config_manager, $entity_type_manager);
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
  }

  /**
   * Ensures bundles that will be deleted are not in use.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   The config import event.
   */
  public function onConfigImporterValidate(ConfigImporterEvent $event) {
    $config_import_settings = $this->configFactory->get('config_import_de.config');

    $delete_entities_enabled = $config_import_settings->get('delete_detected_entities', TRUE);
    $debug_mode_enabled = $config_import_settings->get('debug_mode', FALSE);

    // If delete and debug mode are disabled, return the default config
    // importer validator.
    if (!$delete_entities_enabled && !$debug_mode_enabled) {
      return parent::onConfigImporterValidate($event);
    }

    foreach ($event->getChangelist('delete') as $config_name) {
      // Get the config entity type ID. This also ensure we are dealing with a
      // configuration entity.
      if ($entity_type_id = $this->configManager->getEntityTypeIdByName($config_name)) {
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        // Does this entity type define a bundle of another entity type.
        if ($bundle_of = $entity_type->getBundleOf()) {
          // Work out if there are entities with this bundle.
          $bundle_of_entity_type = $this->entityTypeManager->getDefinition($bundle_of);
          $bundle_id = ConfigEntityStorage::getIDFromConfigName($config_name, $entity_type->getConfigPrefix());
          $entity_query = $this->entityTypeManager->getStorage($bundle_of)->getQuery();
          $entity_ids = $entity_query->condition($bundle_of_entity_type->getKey('bundle'), $bundle_id)
            ->accessCheck(FALSE)
            ->execute();

          if (!empty($entity_ids)) {
            // Only show the error if entity deletion is not enabled.
            if (!$delete_entities_enabled) {
              $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($bundle_id);
              $event->getConfigImporter()->logError($this->t('Entities exist of type %entity_type and %bundle_label %bundle. These entities need to be deleted before importing.', [
                '%entity_type' => $bundle_of_entity_type->getLabel(),
                '%bundle_label' => $bundle_of_entity_type->getBundleLabel(),
                '%bundle' => $entity->label(),
              ]));
            }

            foreach ($entity_ids as $entity_id) {
              $entity = $this->entityTypeManager
                ->getStorage($bundle_of_entity_type->id())
                ->load($entity_id);

              if (!$entity instanceof EntityInterface) {
                continue;
              }

              // Delete entities if the option is enabled.
              if ($delete_entities_enabled) {
                $entity->delete();

                // Output a list of entities if debug mode is enabled.
                if ($debug_mode_enabled) {
                  $message = $this->t('Entity of type %entity_type with id %entity_id is deleted.', [
                    '%entity_type' => $bundle_of_entity_type->getLabel(),
                    '%entity_id' => $entity_id,
                  ]);
                  $this->messenger->addWarning($message);
                }

                continue;
              }

              // Only show the error if entity deletion is not enabled.
              if ($debug_mode_enabled) {
                $message = $this->t('Entity of type %entity_type with id %entity_id should be deleted <a href="@delete_url">here</a>.', [
                  '%entity_type' => $bundle_of_entity_type->getLabel(),
                  '%entity_id' => $entity_id,
                  '@delete_url' => $entity->toUrl('delete-form')->setAbsolute()->toString(),
                ]);
                $event->getConfigImporter()->logError($message);
              }
            }
          }
        }
      }
    }
  }

}
