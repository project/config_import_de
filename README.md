# Configuration Import - Delete Entities

## Introduction

Configuration management in D8 is amazing, and saves us a lot of effort. But
one annoying thing is when it complains about entities that need to be deleted
before a bundle/content-type/entity-type is deleted. When that happens,
you cannot import any configurations until you've manually deleted the
aforementioned entities.

This can be a problem if this happen during an automated configuration import
as part of deployment (or similar). And it can when switching between branches
on a project (where each project may have an entity type not on other branches
yet), and when deploying changes with systems like Jenkins.

It may also not be trivial to find the entities and delete them (depending on
what type of entities they are).
This module allows you to automatically delete entities that may otherwise stop
a configuration import because their bundle/entity type is being deleted.

This can be very helpful when this is part of an automated process for
deployment.

The module also has a "debug" mode, which would list the entity type:ID when
they are detected, even if automated deletion is disabled. This makes it easier
to delete them or at least review them before deciding what to do with them.


## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- Dieter Holvoet - [DieterHolvoet](https://www.drupal.org/u/dieterholvoet)
- Khaled Zaidan - [khaled.zaidan](drupal.org/u/khaledzaidan)
